<?php

namespace Drupal\editablefields\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\BaseFormIdInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EditableFieldsForm.
 */
class EditableFieldsForm extends FormBase implements BaseFormIdInterface {

  /**
   * Entity updated in the form.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Field name.
   *
   * @var string
   */
  protected $field_name;

  /**
   * Form mode.
   *
   * @var string
   */
  protected $form_mode;

  /**
   * Drupal\editablefields\services\EditableFieldsHelperInterface definition.
   *
   * @var \Drupal\editablefields\services\EditableFieldsHelperInterface
   */
  protected $editablefieldsHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->editablefieldsHelper = $container
      ->get('editablefields.helper');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return $this->getBaseFormId() . '_' . $this->prepareUniqueFormId();
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return 'editablefields_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $field = $this->field_name;
    $form_display = $this->getFormDisplay();
    $is_admin = $this->editablefieldsHelper->isAdmin();

    if (empty($form_display) || !$form_display->id()) {
      if ($is_admin) {
        return [
          '#markup' => $this->t('Form mode @mode missing', [
            '@mode' => $this->form_mode,
          ]),
        ];
      }
      return [];
    }

    // Get the field widget from the form mode.
    $component = $form_display->getComponent($field);
    if (!$component) {
      if ($is_admin) {
        return [
          '#markup' => $this->t('The field @field is missing in the @mode', [
            '@field' => $field,
            '@mode' => $this->form_mode,
          ]),
        ];
      }
      return [];
    }

    // Add #parents to avoid error in WidgetBase::form.
    $form['#parents'] = [];

    // Get widget and prepare values for it.
    $widget = $form_display->getRenderer($field);
    $items = $this->entity->get($field);
    $items->filterEmptyItems();

    // Get a widget form.
    $form[$field] = $widget->form($items, $form, $form_state);
    $form[$field]['#access'] = $items->access('edit');

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
      '#ajax' => [
        'callback' => [$this, 'ajaxCallback'],
        'wrapper' => str_replace('_', '-', $this->getFormId()),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
    $entity = $this->entity;
    $field = $this->field_name;
    $form_display = $this->getFormDisplay();

    if (!$form_display || !$form_display->id()) {
      return;
    }

    // Update the entity.
    if ($component = $form_display->getComponent($field)) {
      $widget = $form_display->getRenderer($field);
      $items = $entity->get($field);
      $items->filterEmptyItems();
      $widget->extractFormValues($items, $form, $form_state);
      $entity->save();
    }
  }

  /**
   * Editable field ajax callback.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return array
   *   Updated form.
   */
  public function ajaxCallback(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getErrors()) {
      $form['confirm_message'] = [
        '#markup' => $this->t('Updated'),
      ];
    }
    return $form;
  }

  /**
   * Loads a form display mode.
   *
   * @return \Drupal\Core\Entity\Display\EntityFormDisplayInterface|NULL
   *   Display mode.
   */
  public function getFormDisplay() {
    return $this->editablefieldsHelper->getFormDisplay(
      $this->entity,
      $this->form_mode
    );
  }

  /**
   * Set defaults to be used for unique form ID.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Edited entity.
   * @param $field_name
   *   Field name.
   * @param $form_mode
   *   Form mode.
   */
  public function setDefaults(EntityInterface $entity, $field_name, $form_mode = 'default') {
    $this->entity = $entity;
    $this->field_name = $field_name;
    $this->form_mode = $form_mode;
  }

  /**
   * Set unique form id.
   *
   * @return string
   *   Unique part of the form ID.
   */
  public function prepareUniqueFormId() {
    $entity = $this->entity;

    $parts = [
      $entity->getEntityTypeId(),
      $entity->bundle(),
      $entity->id(),
      $this->field_name,
      $this->form_mode,
    ];

    return implode('_', $parts);
  }

}
