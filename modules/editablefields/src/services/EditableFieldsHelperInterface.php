<?php

namespace Drupal\editablefields\services;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface EditableFieldsHelperInterface.
 */
interface EditableFieldsHelperInterface {

  /**
   * Editablefields formatter ID.
   */
  const FORMATTER_ID = 'editablefields_formatter';

  /**
   * Use "editablefields" permission.
   */
  const PERMISSION = 'use editablefields';

  /**
   * Editablefields admin permission.
   */
  const ADMIN_PERMISSION = 'administer editablefields';

  /**
   * Editablefields form class.
   */
  const FORM_CLASS = 'Drupal\editablefields\Form\EditableFieldsForm';

  /**
   * Default form mode.
   */
  const DEFAULT_FORM_MODE = 'default';

  /**
   * Make "editablefields" formatter available for all the field types.
   *
   * @param array $info
   *   An array of information on existing formatter types, as collected by the
   *   annotation discovery mechanism.
   */
  public function formatterInfoAlter(array &$info);

  /**
   * Get machine names of all the field types.
   *
   * @return array
   *   Array of all field types machine names.
   */
  public function getAllFieldTypes();

  /**
   * Checks if the user can use "editablefields" formatter.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object.
   *
   * @return bool
   *   TRUE if the user can use "editablefields" formatret.
   */
  public function checkAccess(EntityInterface $entity);

  /**
   * Check if the user has administer permission.
   *
   * @return bool
   *   TRUE if the user has administer permission.
   */
  public function isAdmin();

  /**
   * Prepares a render array of the editable field form.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object.
   * @param string $field_name
   *   Field name.
   * @param string $form_mode
   *   Form mode.
   *
   * @return array
   *   Form render array.
   */
  public function getForm(EntityInterface $entity, string $field_name, string $form_mode);

  /**
   * Loads entity form display.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object.
   * @param $form_mode
   *   Form mode.
   *
   * @return \Drupal\Core\Entity\Display\EntityFormDisplayInterface|NULL
   */
  public function getFormDisplay(EntityInterface $entity, $form_mode);

  /**
   * Helper method to prepare the list of the form modes.
   *
   * @return array
   *   Array of form modes.
   */
  public function getModesOptions();

}
