<?php

namespace Drupal\editablefields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\editablefields\services\EditableFieldsHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'editablefields_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "editablefields_formatter",
 *   label = @Translation("Editable field"),
 *   field_types = {}
 * )
 */
class EditableFieldsFieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\editablefields\services\EditableFieldsHelperInterface definition.
   *
   * @var \Drupal\editablefields\services\EditableFieldsHelperInterface
   */
  protected $editablefieldsHelper;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EditableFieldsHelperInterface $editablefieldsHelper) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );
    $this->editablefieldsHelper = $editablefieldsHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\editablefields\services\EditableFieldsHelperInterface $editablefields_helper */
    $editablefields_helper = $container->get('editablefields.helper');
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $editablefields_helper
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'form_mode' => 'default',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
        'form_mode' => [
          '#type' => 'select',
          '#title' => $this->t('Form mode'),
          '#default_value' => $this->getSetting('form_mode'),
          '#options' => $this->editablefieldsHelper->getModesOptions(),
          '#description' => $this
            ->t('The widget of the selected form mode will be used.'),
        ],
      ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [
      $this->t('Form mode: @form_mode', [
        '@form_mode' => $this->getSetting('form_mode'),
      ]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $items->getEntity();

    if (!$this->editablefieldsHelper->checkAccess($entity)) {
      return [];
    }

    return [
      $this->editablefieldsHelper->getForm(
        $entity,
        $this->fieldDefinition->getName(),
        $this->getSetting('form_mode')
      ),
    ];
  }

}
