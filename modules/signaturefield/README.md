Signature (Field)
=================

Provides a signature form element and field.


## Requirements

* [Signature Pad](https://github.com/szimek/signature_pad)


## Installation

Start by adding asset-packagist to the repositories section your `composer.json` file:

```json
{
    "repositories": {
        "asset-packagist": {
            "type": "composer",
            "url": "https://asset-packagist.org"
        }
    }
}
```

Next download the latest release and its dependencies using `composer require drupal/signaturefield`.

Now you can simply enable the module as described in the [the module installation guide](https://www.drupal.org/docs/extending-drupal/installing-modules).


## Usage

### Form element

You can add a signature element in a custom form using following code:

```php
$form['signature'] = [
  '#type' => 'signature',
  '#title' => t('Sign here'),
  '#width' => '400', // Width in pixels, defaults to 400.
  '#height' => '200', // Height in pixels, defaults to 200.
  '#pen_color' => 'black',  // Color used to draw the lines. Can be any color format accepted by context.fillStyle. Defaults to "black".
  '#background_color' => 'rgba(0, 0, 0, 0)', // Color used to clear the background. Can be any color format accepted by context.fillStyle. Defaults to "rgba(0,0,0,0)" (transparent black).
  '#default_value' => NULL, // Signature image as date URI.
];
```

The submitted value will be the signature as [a data URI](https://en.wikipedia.org/wiki/Data_URI_scheme), e.g. `data:image/png;base64,iVBORw0KGgoAAA...`.


### Field

You can create a `Signature` field that will store the signatures as PNG image date URIs.
