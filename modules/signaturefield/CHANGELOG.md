Changelog
=========

All notable changes to the Signature (Field) module.


## 1.0.0 (2021-09-27):

First release.
