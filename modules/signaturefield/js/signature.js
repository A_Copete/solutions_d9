/**
 * @file
 * The signature library.
 */

(function (window, $, Drupal) {

  Drupal.behaviors.signaturefield = {
    attach: function (context, settings) {
      $('canvas').once('signaturefield').each(function () {
        var canvasId = $(this).attr('id');
        if (!canvasId || !canvasId in settings.signaturefield) {
          return;
        }

        var canvas = this;
        var config = settings.signaturefield[canvasId];
        var valueElement = $('#' + config.valueId);
        var value = valueElement.val();

        // Create the signature pad.
        var signaturePad = null;
        signaturePad = new SignaturePad(canvas, {
          penColor: config.penColor,
          backgroundColor: config.backgroundColor,
          onEnd: function () {
            valueElement.val(signaturePad.toDataURL());
          }
        });

        // Bind and trigger the resize callback.
        var resizeCanvas = function () {
          // Resize the canvas.
          var ratio = Math.max(window.devicePixelRatio || 1, 1);
          canvas.width = canvas.offsetWidth * ratio;
          canvas.height = canvas.offsetHeight * ratio;
          canvas.getContext('2d').scale(ratio, ratio);

          // Clear the signature.
          signaturePad.clear();
          valueElement.val('');
        };

        window.addEventListener('resize', resizeCanvas);
        resizeCanvas();

        // Populate the value.
        if (value.indexOf('data:image/') === 0) {
          signaturePad.fromDataURL(value);
          valueElement.val(value);
        }

        // Bind the clear signature link.
        $('a[href="#' + canvasId + '"]').click(function () {
          signaturePad.clear();
          valueElement.val('');
          $(this).blur();

          return false;
        });
      });
    }
  };

})(window, jQuery, Drupal);
