<?php

namespace Drupal\signaturefield\Element;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;

/**
 * Provides a form element for a signature.
 *
 * @FormElement("signature")
 */
class Signature extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#input' => TRUE,
      '#width' => '400',
      '#height' => '200',
      '#pen_color' => 'black',
      '#background_color' => 'rgba(0, 0, 0, 0)',
      '#default_value' => NULL,
      '#element_validate' => [
        [static::class, 'validateElement'],
      ],
      '#process' => [
        [static::class, 'processElement'],
      ],
      '#theme_wrappers' => [
        'form_element',
      ],
    ];
  }

  /**
   * Validate a signature element.
   *
   * @param array $element
   *   The element being validated.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  public static function validateElement(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if ($element['#value'] === '') {
      return;
    }

    if (strpos($element['#value'], 'data:image/png;base64,') !== 0) {
      $form_state->setError($element, t('No valid signature has been submitted.'));
      return;
    }

    // Create a temporary file for validation.
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');
    $directory = $file_system->realpath('temporary://');

    if (!$file = tempnam($directory, 'signature')) {
      $form_state->setError($element, t('The signature image could not be validated.'));
      return;
    }

    try {
      // Save the image.
      file_put_contents($file, base64_decode(mb_substr($element['#value'], 21)));

      // Validate it.
      /** @var \Drupal\Core\Image\ImageFactory $image_factory */
      $image_factory = \Drupal::service('image.factory');
      $image = $image_factory->get($file);

      if (!$image->isValid() || $image->getMimeType() !== 'image/png') {
        $form_state->setError($element, t('No valid signature has been submitted.'));
      }
    }
    finally {
      unlink($file);
    }
  }

  /**
   * Processes a signature element.
   *
   * @param array $element
   *   The element being processed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The altered form element.
   */
  public static function processElement(array $element, FormStateInterface $form_state, array $complete_form) {
    $value_id = Html::getUniqueId('signature-value');
    $canvas_id = Html::getUniqueId('signature-canvas');

    $element['value'] = [
      '#type' => 'hidden',
      '#default_value' => $element['#value'],
      '#parents' => $element['#parents'],
      '#attributes' => [
        'id' => $value_id,
      ],
    ];

    $element['canvas'] = [
      '#theme' => 'signature_canvas',
      '#id' => $canvas_id,
      '#width' => $element['#width'],
      '#height' => $element['#height'],
    ];

    $element['#attached']['library'][] = 'signaturefield/signature';
    $element['#attached']['drupalSettings']['signaturefield'][$canvas_id] = [
      'valueId' => $value_id,
      'penColor' => $element['#pen_color'],
      'backgroundColor' => $element['#background_color'],
    ];

    return $element;
  }

}
