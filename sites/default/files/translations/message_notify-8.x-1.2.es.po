# Spanish translation of Message Notify (8.x-1.2)
# Copyright (c) 2020 by the Spanish translation team
#
msgid ""
msgstr ""
"Project-Id-Version: Message Notify (8.x-1.2)\n"
"POT-Creation-Date: 2020-10-22 20:20+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Language-Team: Spanish\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

msgid "Message"
msgstr "Mensaje"
msgid "Email"
msgstr "Correo electrónico"
msgid "Notify - Email subject"
msgstr "Informar - Asunto correo electrónico"
msgid "Notify - Email body"
msgstr "Informar - cuerpo del correo electrónico"
