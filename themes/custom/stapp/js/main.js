(function ($) {
Drupal.behaviors.behaviorName = {
    attach: function (context, settings) { 

        function showOferta(){
                $("#block-ofertar .field--type-link").click(
                    function(){
                        let dataTitle = $(".field--name-title").text();
                        let dataUser = $(".node__meta a").attr("about").match(/\d+/);
                        
                        $(".formulario-ofertar").show(300);
                        $(".formulario-ofertar").find("#edit-servicio-a-ofertar--2").val(dataTitle);
                        $(".formulario-ofertar").find("#edit-dueno-del-servicio--2").val(dataUser);

                    }
                );
                setTimeout(function(){
                    $("#edit-closer--3").click(
                        function(){
                            $(".formulario-ofertar").toggle(300);
                        }
                    );
                }, 1500);

        }

        function adjOferta(){
           if($(".row-ofertantes").length > 0 ){
                setTimeout(
                    function(){
                        $(".row-ofertantes").append('<td><button class="btn btn-success btn-adjudicar">Adjudicar</button></td>');
                        $(".btn-adjudicar").click(function(){ 
                            var ordenN = $(".field--name-field-orden-numero .field__item").text();
                            var peticionN = $(".field--name-field-tipo-de-peticion .field__item a").text();
                            var ubicacionN =  $(".field--name-field-ubicacion .field__item:nth-child(2) a").text() +" - "+ $(".field--name-field-ubicacion .field__item:nth-child(1) a").text();
                            var priceN = "$" + $(this).closest(".row-ofertantes").find(".views-field-webform-submission-value-2").text().match(/\d+/) + " COP";
                            var docN = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname.split('/')[0] + $(this).closest(".row-ofertantes").find(".views-field-webform-submission-value-1 a").attr("href");
                            var userN = $(this).closest(".row-ofertantes").find(".views-field-uid a").attr("href").match(/\d+/);
                            var activoData = $(".field--name-field-adicionar-activo .field__item").text();
                            var serialData = $(".field--name-field-serial-del-activo .field__item").text();
                            $(".formulario-adjudicar").find("form").trigger("reset");
                            $("#edit-orden-numero--2").val(ordenN); 
                            $("#edit-tipo-de-peticion--2").val(peticionN); 
                            $("#edit-ubicacion--2").val(ubicacionN); 
                            $("#edit-propuesta-economica--2").val(priceN);
                            $("#edit-propuesta-economica--2").parent().hide(); 
                            $("#edit-documento-tecnico--4").val(docN); 
                            $("#edit-proponente--2").val(userN); 
                            $("#edit-activo-asociado--2").val(activoData);
                            $("#edit-activo-asociado--2").parent().hide();
                            $("#edit-serial-del-activo--2").val(serialData);
                            $("#edit-serial-del-activo--2").parent().hide();
                            $(".formulario-adjudicar").show(300);
                        });
                    }, 2000
                );

           }
        }
        function filterOferta(){
            var contentFilter = $(".block-views-blockofertas-block-1");
            var titleFilter = $("#block-stapp-page-title .field--name-title").text();
            contentFilter.find("#edit-webform-submission-value").val(titleFilter);
            contentFilter.find(".form-submit").click();
        }
        function activosView(){
            let contentActivo = $("#edit-asociar-con-activo");
            let contentSerial = $("#edit-serial-del-activo--2");
            let contentActivo2 = $("#edit-activo-asociado");
            $(".page-view-mis-activos").find("#block-webform-9").hide();
            if(contentActivo.length > 0 || contentActivo2.length > 0){
                contentActivo.click(
                    function(){
                        $("#block-views-block-mis-activos-block-1").addClass("active");
                        $("#block-views-block-mis-activos-block-1").append('<div class="close"></div>');
                        $(".close").click(
                            function(){
                                $("#block-views-block-mis-activos-block-1").removeClass("active");
                                $(".close").remove();
                            }
                        );
                    }
                );
   
                $(".selector").click(
                    function(){
                        let data = $(this).closest(".table").find(".views-field-title a").text();
                        let serialData = $(this).closest(".table").find(".views-field-field-serial").text();
                        let activoD = $(this).closest("tr").find(".views-field-title a").text();
                        let serialD = $(this).closest("tr").find(".views-field-field-serial").text().match(/\d+/);
                        contentSerial.val(serialData);
                        contentActivo.val(data);
                        $("#edit-activo-asociado").val(activoD);
                        $("#edit-serial-del-activo").val(serialD);

                        $("#block-views-block-mis-activos-block-1").removeClass("active");
                        $(".close").remove();
                    }
                );
                $(".selector-peticion").click(
                    function(){
                        let activoD = $(this).closest("tr").find(".views-field-title a").text();
                        let serialD = $(this).closest("tr").find(".views-field-field-serial").text().match(/\d+/);
                        $("#edit-asociar-con-activo").val(activoD);
                        $("#edit-asociar-con-activo").parent().hide();
                        $("#edit-serial-del-activo").val(serialD);
                        $("#edit-serial-del-activo").parent().hide();
                        $(".page-view-mis-activos").find("#block-webform-9").show();
                    }
                );

            }
        }
        function tecnicoViews(){
            var tecVar = $("#block-views-block-mis-servicios-asignados-block-1").length;
            var empVar = $("#block-views-block-mis-servicios-asignados-block-2").length;
            var dataTec =  $("#block-views-block-mis-servicios-asignados-block-1").find("tbody tr").length;
            var dataE =  $("#block-views-block-mis-servicios-asignados-block-2").find("tbody tr").length;
            $("#block-views-block-mis-servicios-asignados-block-2").hide();
            if(tecVar > 0 && dataTec > 0){
                $("#main-wrapper").prepend('<span class="alert services-4-work">Tienes <strong>'+"("+ dataTec +")"+'</strong> servicios pendientes</span>');
            }
            if(empVar > 0 && dataE > 0){
                $("#main-wrapper").prepend('<span class="alert services-4-work">Tienes <a href="/mis-servicios-asignados"><strong>'+"("+ dataE +")"+'</strong></a> servicios pendientes</span>');
            }
        }
        function formsService(){
            var pageContent = $(".node--type-orden-de-servicio").length;
            if(pageContent > 0){
                $(".block-webform h2").click(
                    function(){
                        $(".block-webform .content").hide();
                        $(this).next().toggle();
                    }
                );
            }
        }
        function mDashboard(){
            if($("#block-views-block-dashboard-block-1 .closer-dash").length < 1 ){
                $("#block-views-block-dashboard-block-1 h2").append('<div class="closer-dash">Mostrar/Ocultar</div>');
            }
            $("#block-views-block-dashboard-block-1 .closer-dash").click(
                function(){
                    $("#block-views-block-dashboard-block-1 .content").toggleClass("active");
                }
            );
            $("#edit-activo-asociado").click(
                function(){
                    $("#block-views-block-mis-activos-block-1").addClass("active");
                }
            );
        }
        function loader(){
           setTimeout(
               function(){
                $(".loader-page").hide(500);
                filterOferta();
                adjOferta();
               }, 1000
           ); 
        }


        $(document).ready(
            function(){
                setTimeout(()=>{        
                    $("#edit-closer--4").click(
                        function(){
                            $(".formulario-adjudicar").hide(300);
                });}, 2000);
                $(".field--name-field-imagenes .field__items").slick(
                    {
                        autoplay: true,
                        autoplaySpeed: 1000,
                    }
                );
                loader();
                showOferta();
                activosView();
                setTimeout(()=>{tecnicoViews();}, 2000);
                formsService();
                mDashboard();
                //filterCalendar();
            }
        );
    }
};
})(jQuery);